package com.citysavings.online_turnoverapplication.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.citysavings.online_turnoverapplication.room.daos.dao_profile;
import com.citysavings.online_turnoverapplication.room.entities.ent_profile;

@Database(entities = {ent_profile.class},version = 1, exportSchema = false)
public abstract class ContextDB extends RoomDatabase {
    private  static  ContextDB instance;

    public abstract dao_profile dao_profile();

    public static ContextDB getAppDatabase(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    ContextDB.class,
                    "ncovdb")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }
}
