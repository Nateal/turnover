package com.citysavings.online_turnoverapplication.room.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ent_profile {
    @PrimaryKey
    @NonNull
    public String key;
    @NonNull
    public String FullName;

    public String SecretCode;
    @NonNull
    public String Email;
    @NonNull
    public String Phone;

    @NonNull
    public String getKey() {
        return key;
    }

    public void setKey(@NonNull String key) {
        this.key = key;
    }

    @NonNull
    public String getFullName() {
        return FullName;
    }

    public void setFullName(@NonNull String fullName) {
        FullName = fullName;
    }

    public String getSecretCode() {
        return SecretCode;
    }

    public void setSecretCode(String secretCode) {
        SecretCode = secretCode;
    }

    @NonNull
    public String getEmail() {
        return Email;
    }

    public void setEmail(@NonNull String email) {
        Email = email;
    }

    @NonNull
    public String getPhone() {
        return Phone;
    }

    public void setPhone(@NonNull String phone) {
        Phone = phone;
    }
}
