package com.citysavings.online_turnoverapplication.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.citysavings.online_turnoverapplication.room.entities.ent_profile;

import java.util.List;

@Dao
public interface dao_profile {
    @Insert
    void insert(ent_profile... entity);

    @Update
    void update(ent_profile... entity);

    @Delete
    void dalete(ent_profile... entity);

    @Query("Delete from ent_profile")
    void deleteAll();

    @Query("Select * FROM ent_profile where `key`=:key")
    ent_profile getById(String key);

    @Query("Select * FROM ent_profile")
    ent_profile[] getAll();

    @Query("select * from ent_profile order by fullName")
    LiveData<List<ent_profile>> getLiveAll();
}
