﻿using ClassLibrary_Sample;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace WebApplication_Sample.EF
{
    public class MyContext : DbContext
    {
        public MyContext(DbContextOptions<MyContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<Virus>().HasIndex(x => new { x.Title }).IsUnique();
        }

        public DbSet<Virus> Viruses { get; set; }
    }
}