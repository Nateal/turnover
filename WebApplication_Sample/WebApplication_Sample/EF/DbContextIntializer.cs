﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace WebApplication_Sample.EF
{
    public static class DbContextIntializer
    {
        public static void Initialize(MyContext context, IHostingEnvironment env)
        {
            context.Database.Migrate();
            context.Database.EnsureCreated();
        }
    }
}